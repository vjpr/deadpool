express = require("express")
routes = require("./src/routes")
hamlc = require("haml-coffee")
app = module.exports = express.createServer()
app.register 'hamlc', hamlc
app.configure ->
  app.set "views", "views"
  app.set "view engine", "hamlc"
  app.use express.bodyParser()
  app.use express.methodOverride()
  app.use app.router
  app.use express.static(__dirname + "/public")

app.configure "development", ->
  app.use express.errorHandler(
    dumpExceptions: true
    showStack: true
  )

app.configure "production", ->
  app.use express.errorHandler()

app.get "/", routes.index
port = process.env.PORT || 3012
app.listen port
console.log "Express server listening on port %d in %s mode", app.address().port, app.settings.env
